package br.com.mastertech.lanchonete.lanches;

import br.com.mastertech.lanchonete.security.OAuth2FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "lanches", url = "http://localhost:8080", configuration = OAuth2FeignConfiguration.class)
public interface LancheClient {

    @GetMapping
    List<Lanche> getLanches();

}
