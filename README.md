# Demonstração OAuth2

Esses 3 projetos tem o objetivo de demonstrar o funcionamento do OAuth2 em uma estrutura de microsserviços, sendo eles:

## Authorization Server (authserver):
Servidor de autorização do OAuth2, ele é o responsável por gerenciar os acessos das aplicações (clients) aos recursos (Resource Server) e emitir os tokens de acesso.

Obs: Não vimos em aula a explicação detalhada desse projeto, estuda-lo é um bonus.

## Lanche (lanche):
Uma aplicação simples simulando um Resource Server, quando requisitada retorna um objeto lanche contendo o nome do usuário logado como dono e o tipo do lanche com o valor fixo de "X-Salada".

## Lanchonete (lanchonete)
Uma aplicação simples simulando um Resource Server, quando requisitada retorna um objeto lanchonete contendo o nome do usuário logado como dono e o lanche retornado pelo microsserviço Lanche.

Obs: O interessante desse projeto é a mecanica que ele tem para conseguir chamar o Lanche via Feign sendo que o lanche está protegido via OAuth2.